use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct VariationAttribute {
    pub id: u32,
    pub name: String,
    pub option: String,
}
