use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct ProductImage {
    id: u32,
    pub src: String,
    name: String,
    alt: String,
}
