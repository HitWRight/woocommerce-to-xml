mod models;
mod woocommerce_model;

use crate::models::product::product_attribute::ProductAttribute;
use crate::woocommerce_model::get_data;
use clap;
use clap::{crate_authors, crate_version, App, Arg, SubCommand};
use futures::join;
use log::{debug, error, info, warn};
use std::io::Write;

use crate::models::attribute::Attribute;
use crate::models::category::Category;
use crate::models::product::variation::Variation;
use crate::models::product::Product;

#[tokio::main]
async fn main() {
    env_logger::init();
    info!("Logger started");
    let matches = App::new("Woocommerce to XML file")
        .version(crate_version!())
        .author(crate_authors!())
        .about(
            "Connects to Woocommerce API v3 and converts to an XML file usable for Varle LT\
                (Requires read permissions only)",
        )
        .arg(
            Arg::with_name("url")
                .required(true)
                .short("u")
                .long("url")
                .help("Woocommerce API base URL")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("consumer_key")
                .required(true)
                .short("c")
                .long("consumer-key")
                .help("API consumer key")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("secret_key")
                .required(true)
                .short("s")
                .long("secret-key")
                .help("API secret key")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("output_file")
                .short("o")
                .long("output-file")
                .help("Output file name to where to write")
                .takes_value(true)
                .default_value("output.xml"),
        )
        .get_matches();

    if let (Some(request_url), Some(consumer_key), Some(secret_key), Some(output_file)) = (
        matches.value_of("url"),
        matches.value_of("consumer_key"),
        matches.value_of("secret_key"),
        matches.value_of("output_file")
    ) {
        let mut file = std::fs::File::create(output_file).expect("Failed to create output file.");
        let categories = get_data(&request_url, &consumer_key, &secret_key);
        let products = get_data(&request_url, &consumer_key, &secret_key);

        if let (Ok(categories), Ok(products)) = join!(categories, products) {
            let mut variations = Vec::new();
            let products: Vec<Product> = products;
            for product in &products {
                if product.variations.len() != 0 {
                    info!("Rereiving product attributes: {}", product.id);
                    let product_variations =
                        get_variation(&request_url, &consumer_key, &secret_key, product.id)
                            .await
                            .unwrap();
                    variations.push(product_variations);
                }
            }
            file.write_all(format_xml(categories, products, variations).as_bytes())
                .unwrap();
        }
    }
    info!("Program finished");
}

fn format_xml(
    categories: Vec<Category>,
    products: Vec<Product>,
    attributes: Vec<(u32, Vec<Variation>)>,
) -> String {
    [
        "<root>",
        &format_categories(categories),
        &format_products(products, attributes),
        "</root>",
    ]
    .concat()
}

fn format_categories(categories: Vec<Category>) -> String {
    let mut result = Vec::new();
    result.push("<categories>".to_string());
    result.extend::<Vec<String>>(
        categories
            .into_iter()
            .map(|category| {
                [
                    "<category>",
                    "<id>",
                    &category.id.to_string(),
                    "</id>",
                    "<parent>",
                    &category.parent.to_string(),
                    "</parent>",
                    "<name><![CDATA[",
                    &category.name,
                    "]]></name>",
                    "</category>",
                ]
                .concat()
            })
            .collect(),
    );
    result.push("</categories>".to_string());
    result.concat()
}

fn format_products(products: Vec<Product>, variations: Vec<(u32, Vec<Variation>)>) -> String {
    info!("Formatting products XML");
    let mut result = Vec::<String>::new();
    result.push("<products>".to_string());
    result.extend::<Vec<String>>(
        products
            .into_iter()
            .filter(|product| !product.permalink.contains("post_type=product"))
            .map(|product| {
                let prodid = product.id.clone();
                let prodattr = product.attributes.clone();
                [
                    "<product>".to_string(),
                    format!("<url>{}</url>", product.permalink.replace("&", "&#38;")),
                    format!("<id>{}</id>", product.id),
                    format!(
                        "<categories>{}</categories>",
                        product
                            .categories
                            .into_iter()
                            .map(|category| format!("<category>{}</category>", category.id))
                            .collect::<Vec<String>>()
                            .concat()
                    ),
                    format!("<title><![CDATA[{}]]></title>", product.name),
                    format!(
                        "<description><![CDATA[{}]]></description>",
                        if product.description.len() != 0 {
                            product.description
                        } else {
                            product.short_description
                        }
                    ),
                    format!("<price>{}</price>", product.price),
                    format!("<price_old>{}</price_old>", product.regular_price),
                    format!("<quantity>{}</quantity>", {
                        match product.stock_quantity {
                            Some(val) => {
                                if val == 0 {
                                    "0".to_string()
                                } else {
                                    val.to_string()
                                }
                            }
                            None => "".to_string(),
                        }
                    }),
                    format!("<warranty></warranty>"),
                    format!("<weight>{}</weight>", product.weight),
                    {
                        match prodattr.iter().find(|x| x.name == "Gamintojas") {
                            Some(val) => format!(
                                "<manufacturer><![CDATA[{}]]></manufacturer>",
                                val.options.first().unwrap_or(&"Undefined".to_string())
                            ),
                            None => "".to_string(),
                        }
                    },
                    if product.images.len() == 0 {
                        "".to_string()
                    } else {
                        format!(
                            "<images>{}</images>",
                            product
                                .images
                                .into_iter()
                                .map(|image| format!("<image>{}</image>", image.src))
                                .collect::<Vec<String>>()
                                .concat()
                        )
                    },
                    format_variants(&prodattr, prodid, &variations),
                    if product
                        .attributes
                        .iter()
                        .filter(|x| x.name != "Gamintojas" && x.variation != Some(true))
                        .count()
                        == 0
                    {
                        "".to_string()
                    } else {
                        format!(
                            "<attributes>{}</attributes>",
                            product
                                .attributes
                                .into_iter()
                                .filter(|x| x.name != "Gamintojas" && x.variation != Some(true))
                                .map(|attr| attr
                                    .options
                                    .iter()
                                    .map(|opt| format!(
                                        "<attribute title=\"{}\"><![CDATA[{}]]></attribute>",
                                        attr.name, opt
                                    ))
                                    .collect::<Vec<String>>()
                                    .concat())
                                .collect::<Vec<String>>()
                                .concat()
                        )
                    },
                    "</product>".to_string(),
                ]
                .concat()
            })
            .collect(),
    );
    result.push("</products>".to_string());
    result.concat()
}

fn format_variants(
    product_attributes: &Vec<ProductAttribute>,
    product_id: u32,
    variations: &Vec<(u32, Vec<Variation>)>,
) -> String {
    if product_attributes
        .iter()
        .filter(|x| x.variation == Some(true))
        .count()
        == 0
    {
        "".to_string()
    } else {
        format!(
            "<variants>{}</variants>",
            match variations.iter().find(|x| x.0 == product_id) {
                Some(val) => val.1.iter().map(|x| format!("<variant><attributes>{}</attributes><price>{}</price><quantity>{}</quantity></variant>",
                                                          x
                                                          .attributes
                                                          .iter()
                                                          .map(|y| format!("<attribute title=\"{}\"><![CDATA[{}]]></attribute>",
                                                                           y.name,
                                                                           y.option))
                                                          .collect::<Vec<String>>()
                                                          .concat(),
                                                          x.price,
                                                          x.stock_quantity
                                                          .unwrap_or(0)
                                                          .to_string()))
                    .collect::<Vec<String>>()
                    .concat(),
                None => "".to_string()
            }
        )
    }
}

async fn get_variation(
    url: &str,
    consumer_key: &str,
    secret_key: &str,
    product_id: u32,
) -> Result<(u32, Vec<Variation>), reqwest::Error> {
    info!("Retrieving variations for: {}", product_id);
    let request_path = format!(
        "/wp-json/wc/v3/products/{}/variations?per_page=50&page=",
        product_id
    );
    let mut result = Vec::<Variation>::new();

    let request_url = [url, &request_path, "1"].concat();
    let client = reqwest::Client::new()
        .get(&request_url)
        .basic_auth(consumer_key, Some(secret_key))
        .send()
        .await?;
    let text = client.text().await?;

    match serde_json::from_str(&text) {
        Ok(result_addition) => result.extend::<Vec<Variation>>(result_addition),
        Err(error) => error!("Failed to parse data({}): {}", error, text),
    }

    Ok((product_id, result))
}
