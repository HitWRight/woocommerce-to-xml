use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct ProductCategory {
    pub id: u32,
    pub name: String,
}
