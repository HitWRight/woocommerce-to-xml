use log::{info};

pub trait WoocommerceModel {
    fn request_path() -> &'static str;
}

pub async fn get_data<T>(
    url: &str,
    consumer_key: &str,
    secret_key: &str,
) -> Result<Vec<T>, reqwest::Error>
where
    for<'de> T: serde::Deserialize<'de> + WoocommerceModel,
{
    let request_path = T::request_path();
    let mut result = Vec::<T>::new();
    let request_url = [url, &request_path, "?per_page=50&page=", "1"].concat();
    info!("Retrieving data from {}", request_url);
    let client = reqwest::Client::new()
        .get(&request_url)
        .basic_auth(consumer_key, Some(secret_key))
        .send()
        .await?;
    let pagecount = client.headers()["x-wp-totalpages"]
        .to_str()
        .expect("Failed to find x-wp-totalpages header")
        .parse::<i32>()
        .expect("Failed to parse w-wp-totalpages count");
    result.extend::<Vec<T>>(
        serde_json::from_str(&client.text().await?).expect("Failed to parse data"),
    );
    for i in 2..=pagecount {
        let request_url = [url, &request_path, "?per_page=50&page=", &i.to_string()].concat();
        let client = reqwest::Client::new()
            .get(&request_url)
            .basic_auth(consumer_key, Some(secret_key))
            .send()
            .await?;
        result.extend::<Vec<T>>(
            serde_json::from_str(&client.text().await?).expect("Failed to parse data"),
        );
        info!("{}/{}", i*50, pagecount*50 );
    }
    Ok(result)
}
