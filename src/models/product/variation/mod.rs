mod attribute;
use crate::models::product::variation::attribute::VariationAttribute;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Variation {
    id: u32,
    // date_created: String,
    // date_created_gmt: String,
    // date_modified: String,
    // date_modified_gmt: String,
    description: String,
    // permalink: String,
    // sku: String,
    pub price: String,
    regular_price: Option<String>,
    sale_price: Option<String>,
    // date_on_sale_from: String,
    // date_on_sale_from_gmt: String,
    // date_on_sale_to: String,
    // date_on_sale_to_gmt: String,
    // on_sale: bool,
    // status: String,
    // purchasable: bool,
    // r#virtual: bool,
    // downloadable: bool,
    // download_limit: u32,
    // download_expiry: u32,
    // tax_status: String,
    // tax_class: String,
    // manage_stock: bool,
    pub stock_quantity: Option<u32>,
    // stock_status: String,
    // backorders: String,
    // backorders_allowed: bool,
    // backordered: bool,
    // weight: string,
    // dimensions:
    // shipping_class: String,
    // shipping_class_id: String,
    // image:
    pub attributes: Vec<VariationAttribute>
    // menu_order: u32,
    // meta_data:
}
