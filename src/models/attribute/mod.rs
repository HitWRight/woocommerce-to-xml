use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Attribute {
    pub id: u32,
    pub name: String,
    pub slug: String,
    pub r#type: String,
    pub order_by: String,
    pub has_archives: bool
}
