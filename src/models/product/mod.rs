use crate::woocommerce_model::WoocommerceModel;
use serde::{Deserialize, Serialize};
pub mod product_attribute;
pub mod product_category;
pub mod product_image;
pub mod variation;

use product_attribute::ProductAttribute;
use product_category::ProductCategory;
use product_image::ProductImage;

#[derive(Serialize, Deserialize, Debug)]
pub struct Product {
    pub permalink: String,
    pub id: u32,
    pub categories: Vec<ProductCategory>,
    pub name: String,
    pub description: String,
    pub short_description: String,
    pub sale_price: String,
    pub price: String,
    pub stock_quantity: Option<u32>,
    //warrant
    pub weight: String,
    //manufacturer
    pub images: Vec<ProductImage>,
    pub attributes: Vec<ProductAttribute>,
    // date_created: String,
    // date_created_gmt: String,
    // date_modified: String,
    // date_modified_gmt: String,
    // r#type: String,
    // status: String,
    // featured: bool,
    // catalog_visibility: String,
    // description: String,
    // short_description: String,
    // sku: String,
    // price: String,
    pub regular_price: String,
    // sale_price: String,
    // date_on_sale_from: String,
    // date_on_sale_from_gmt: String,
    // date_on_sale_to: String,
    // date_on_sale_to_gmt: String,
    // price_html: String,
    // on_sale: bool,
    // purchasable: bool,
    // total_sales: u32,
    // r#virtual: bool,
    // downloadable: bool,
    // downloads: Vec<Download>,
    pub variations: Vec<u32>
}

impl WoocommerceModel for Product {
    fn request_path() -> &'static str {
        "/wp-json/wc/v3/products"
    }
}
