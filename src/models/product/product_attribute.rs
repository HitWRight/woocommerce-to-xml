use serde::{Deserialize, Serialize};

#[derive(Clone, Serialize, Deserialize, Debug)]
pub struct ProductAttribute {
    pub id: u32,
    pub name: String,
    position: u32,
    pub visible: bool,
    pub variation: Option<bool>,
    pub options: Vec<String>
}

