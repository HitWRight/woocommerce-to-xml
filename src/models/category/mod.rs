use crate::woocommerce_model::WoocommerceModel;
use serde::{Deserialize, Serialize};

#[derive(Serialize, Deserialize, Debug)]
pub struct Category {
    pub id: u32,
    pub name: String,
    pub slug: String,
    pub parent: u32,
    // description: String,
    // display: String,
    // menu_order: u32,
    // count: u32
}

impl WoocommerceModel for Category {
    fn request_path() -> &'static str {
        "/wp-json/wc/v3/products/categories"
    }
}
